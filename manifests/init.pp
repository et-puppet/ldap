# Class: ldap
# ===========================
#
# Build a master or replica LDAP server
#
# Examples
# --------
#
# @example
#    class { 'ldap': }
#
# Authors
# -------
#
# Ian Douglas <idouglas@stanford.edu>
#
# Copyright
# ---------
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior
# University
#
class ldap (
  $ldap_dirs
) {

  include ldap::packages

  file { '/etc/apt/preferences.d/cyrus-sasl':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/aptprefs/cyrus-sasl",
    before => [ Package['libsasl2-2'], Package['libsasl2-modules-gssapi-mit'] ]
  }

  file { '/etc/apt/preferences.d/ldap':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/aptprefs/ldap",
    before => [ Package['libldap-2.4-2'], Package['slapd'], Package['slapd-dbg'],
                Package['ldap-utils'], Package['libnet-ldap-perl'], Package['libnet-ldapapi-perl'] ]
  }

  file { '/etc/apt/sources.list.d/stanfordlocal.list':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/aptsources/stanfordlocal.list",
    before => [ Package['libldap-2.4-2'], Package['slapd'], Package['slapd-dbg'], 
                Package['ldap-utils'], Package['libnet-ldap-perl'], Package['libnet-ldapapi-perl'],
                Package['libsasl2-2'], Package['libsasl2-modules-gssapi-mit'], Package['stanford-ldap-tools'], 
                Package['libstanford-directory-perl'] ]
  }

  exec { 'aptupdate':
    command => 'apt-get update',
    path    => '/usr/bin/',
    require => [ File['/etc/apt/sources.list.d/stanfordlocal.list'], File['/etc/apt/preferences.d/ldap'], File['/etc/apt/preferences.d/cyrus-sasl'] ],
    before => [ Package['libldap-2.4-2'], Package['slapd'], Package['slapd-dbg'], 
                Package['ldap-utils'], Package['libnet-ldap-perl'], Package['libnet-ldapapi-perl'],
                Package['libsasl2-2'], Package['libsasl2-modules-gssapi-mit'], Package['stanford-ldap-tools'], 
                Package['libstanford-directory-perl'] ]
  }

  file { $ldap_dirs:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/start':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
    source => "puppet:///modules/${module_name}/start_ldap.sh"
  }

  file { '/etc/krb5.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/etc/krb5.conf"
  }

  file { '/etc/systemd/system/krenew.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/etc/krenew.service"
  }

  file { '/usr/bin/krbtkt.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
    source => "puppet:///modules/${module_name}/etc/krbtkt.sh"
  }

  file { '/etc/systemd/system/mdbcp.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/etc/mdbcp.service"
  }

  file { '/usr/bin/mdb_cp.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
    source => "puppet:///modules/${module_name}/etc/mdb_cp.sh"
  }

  file { '/etc/systemd/system/slapd.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/ldap/slapd.service"
  }

  file { '/etc/systemd/system/journalstream.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/etc/journalstream.service"
  }

  file { '/etc/systemd/system/splunkd.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/etc/splunkd.service"
  }

  file { '/usr/bin/r53-register.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0777',
    source => "puppet:///modules/${module_name}/etc/r53-register.sh"
  }

  file { '/usr/bin/r53-register-multi.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0777',
    source => "puppet:///modules/${module_name}/etc/r53-register-multi.sh"
  }

  file { '/usr/bin/prune-asg-dns.sh':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0777',
    source => "puppet:///modules/${module_name}/etc/prune-asg-dns.sh"
  }

  file { '/etc/systemd/system/r53-multiupsert.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/etc/r53-multiupsert.service"
  }

  file { '/etc/ldap/ldap.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/ldap/ldap.conf"
  }

  file { '/etc/default/slapd':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/ldap/slapd"
  }

  file { '/etc/ldap/sasl2/slapd.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/ldap/sasl2/slapd.conf"
  }

  file { '/var/log/ldap':
    ensure => 'link',
    target => '/var/lib/ldap/logs/ldap',
    force  => true
  }

  file { '/var/log/OLD-ldap':
    ensure => 'link',
    target => '/var/lib/ldap/logs/OLD'
  }

  file { '/var/lib/ldap/data.mdb':
    ensure  => absent
  }

  file { '/var/lib/ldap/lock.mdb':
    ensure  => absent
  }

  file { '/etc/ldap/slapd.d/cn=config':
    ensure  => directory,
    purge   => true,
    recurse => true,
    force   => true,
    owner   => 'root',
    group   => 'root',
    mode    => '0750'
  }

}
