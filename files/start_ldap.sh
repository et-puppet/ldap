#!/bin/bash

LDAP_BAK_DIR=${LDAP_BAK_DIR:-/srv/ldap/backup/$LDAP_ROLE}
LDAP_MDB_DIR=${LDAP_MDB_DIR:-/var/lib/ldap}
LDAP_CONF_DIR=${LDAP_CONF_DIR:-/etc/ldap/slapd.d}
LDAP_LOG_LEVEL=${LDAP_LOG_LEVEL:-16383}
LDAP_BASE=${LDAP_BASE:-'dc=stanford,dc=edu'}
MDB_BAK_HISTORY=${MDB_BAK_HISTORY:-4}
MDB_DUMP_FREQ=28800
CONF_BAK_HISTORY=${CONF_BAK_HISTORY:-4}
KRB5_KTNAME=${KRB5_KTNAME:-/etc/ldap.keytab}
KRB5_CCDIR=${KRB5_CCDIR:-/var/tmp/ccache}
KRB5CCNAME=${KRB5CCNAME:-FILE:/var/tmp/ccache/krb5cc_0}
REALM=${REALM:-stanford.edu}
LDAP_CONFIG_SRC=${LDAP_CONFIG_SRC:-/srv/ldap/source_config}
LDAP_REPL_MASTER0=${LDAP_REPL_MASTER0:-'directory-master0.stanford.edu'}
LDAP_REPL_MASTER1=${LDAP_REPL_MASTER1:-'ldap0.stanford.edu'}
LDAP_REPL_SHADOWMASTER=${LDAP_REPL_SHADOWMASTER:-'ldap-prod-cl0.cluster.local'}
LDAP_REPL_MASTER0_RID=${LDAP_REPL_MASTER0_RID:=000}
LDAP_REPL_MASTER1_RID=${LDAP_REPL_MASTER1_RID:=100}
LDAP_REPL_SHADOWMASTER_RID=${LDAP_REPL_MASTER1_RID:=100}
LDAP_REPL_TOPO=${LDAP_REPL_TOPO:='singlemaster'}
LDAP_LOGFILE=/var/lib/ldap/logs/ldap

ACTION=$1


function mdb_load() {

    echo -e "Assessing data state for ldap role: ${LDAP_ROLE}"

    file_rebuild_data=$(ls -t ${LDAP_BAK_DIR}/rebuild/*.db-ldif* 2>/dev/null | head -n 1)
    file_rebuild_config=$(ls -t ${LDAP_BAK_DIR}/rebuild/*config.ldif 2>/dev/null | head -n 1)
    file_data=$(ls -t ${LDAP_BAK_DIR}/mdb/data.mdb* 2>/dev/null | head -n 1)
    configSrc=$(ls -t ${LDAP_CONFIG_SRC}/${LDAP_ROLE}/cn-config_*.ldif 2>/dev/null | head -n 1)
    file_rebuild_force=$(ls ${LDAP_BAK_DIR}/rebuild/force-rebuild 2>/dev/null)  # if this file exists, force rebuild from ldif

    # if either no current snapshot or no config, and, either no data ldif or no config ldif in rebuild directory, exit
    if [[ ( ! -f "$file_data" || ! -f "$configSrc" ) && ( ! -f "$file_rebuild_data"  || ! -f "$file_rebuild_config" ) ]] ; then
        echo "No mdb snapshot found in ${LDAP_BAK_DIR}/mdb and no config/data ldifs to load. Exiting." >&2
        exit 1
    fi
    # if no configuration file found in git-synced source directory, exit
    if [[ ! -f ${configSrc} ]]; then
        echo "No configuration file found in ${LDAP_CONFIG_SRC}/staged/${LDAP_ROLE}. Exiting." >&2
        exit 1
    fi
    # ensure data dump dirs present
    if [ ! -d ${LDAP_BAK_DIR}/mdb ]; then
        echo "data dump directory not present (creating): ${LDAP_BAK_DIR}/mdb" >&2
        mkdir -p ${LDAP_BAK_DIR}/mdb
    fi

    if [ ! -d ${LDAP_BAK_DIR}/mdb/accesslog ]; then
        echo "accesslog dump directory not present (creating): ${LDAP_BAK_DIR}/mdb/accesslog" >&2
        mkdir -p ${LDAP_BAK_DIR}/mdb/accesslog
    fi

    if [ ! -d ${LDAP_BAK_DIR}/config ]; then
        echo "config dump directory not present (creating): ${LDAP_BAK_DIR}/config" >&2
        mkdir -p ${LDAP_BAK_DIR}/config
    fi

    # if either no current snapshot or no config, or, rebuild file is present, set REBUILD flag
    if [[ ( ! -f "$file_data" || ! -f "$configSrc" || -f "$file_rebuild_force" ) ]]; then REBUILD=true; else REBUILD=false; fi

    if [[ "${REBUILD}" == false ]]; then
        # most recent data.mdb in backup
        bakData=$(ls -t ${LDAP_BAK_DIR}/mdb/data.mdb* | head -n 1)
        echo "most recent mdb file in backup: $bakData"
        # checksum of most recent config in configuration source
        configSrc_md5=$(md5sum $configSrc | cut -d ' ' -f 1)
        # checksum of last-loaded config
        if [[ -f ${LDAP_CONF_DIR}/${LDAP_ROLE}-cn-config.checksum ]]; then
            configLast_md5=$(cat ${LDAP_CONF_DIR}/${LDAP_ROLE}-cn-config.checksum | cut -d ' ' -f 2)
        fi
        # restore data and config if newer backup exists
        echo "comparing timestamps of $bakData and ${LDAP_MDB_DIR}/data.mdb"
        if [ ${bakData} -nt ${LDAP_MDB_DIR}/data.mdb ]; then
            echo -e "Backup data is newer than current mdb.\nLoading most recent data and config..."
            find ${LDAP_MDB_DIR} -type f -name '*.mdb' -delete
            stat --format "restoring snapshot '%n' modified on %y" ${bakData}
            cp -v ${bakData} ${LDAP_MDB_DIR}/data.mdb
            if [[ ${LDAP_ROLE} == "master" ]]; then
                bakDataAccessLog=$(ls -t ${LDAP_BAK_DIR}/mdb/accesslog/accesslog.mdb* | head -n 1)
                # restore snapshot of latest accesslog mdb iff it corresponds to most recent data snapshot,
                # ie, it is tagged with the same timestamp, eg, <filename>.mdb.<timestamp>
                if [[ ${bakData##*.} == ${bakDataAccessLog##*.} ]]; then
                    echo -e "Loading accesslog corresponding to most recent data snapshot"
                    stat --format "restoring snapshot of accesslog '%n' modified on %y" ${bakDataAccessLog}
                    cp -v ${bakDataAccessLog} ${LDAP_MDB_DIR}/accesslog/data.mdb
                fi
            fi
        else
            echo "Current mdb is most recent data available."
        fi
        # restore config if checksums differ, or if no checksum file exists for last-loaded config
        if [[ ! -f ${LDAP_CONF_DIR}/${LDAP_ROLE}-cn-config.checksum || $configLast_md5 != $configSrc_md5 ]]; then
            rm -rf ${LDAP_CONF_DIR}/*
            echo -e "comparing configuration checksums"
            echo -e "${configLast_md5}: last-loaded configuration"
            echo -e "${configSrc_md5}: most recent git-synced configuration in ${LDAP_CONFIG_SRC}/${LDAP_ROLE}"
            stat --format "Loading config from ldif '%n' modified on %y having md5sum ${configSrc_md5}" ${configSrc}
            slapadd -v -F ${LDAP_CONF_DIR} -bcn=config -l ${configSrc}
            # test data load success
            if [ "$?" -ne 0 ]; then
                echo "Config data load did not succeed. Exiting." >&2
                # remove restored data since config faulty, forcing reload on next boot
                rm -rfv ${LDAP_MDB_DIR}/*.mdb
                exit 1
            fi
            # write checksum to compare against source on next boot
            echo $(basename $configSrc) $configSrc_md5 > ${LDAP_CONF_DIR}/${LDAP_ROLE}-cn-config.checksum
        else
            echo "Current configuration is most recent available."
        fi
    fi

    if [[ "${REBUILD}" == true ]]; then
        if [[ -f "$file_rebuild_force" ]]; then
            echo -e "Data override file found at '${LDAP_BAK_DIR}/rebuild/force-rebuild' \nIgnoring snapshots and attempting data and config load from ldif."
        else
            echo -e "No current snapshot and config found. \nAttempting data and config load from ldif."
        fi
        if [[ ! -f "$file_rebuild_data"  || ! -f "$file_rebuild_config" ]]; then
            echo "The required data and config ldif files not found in ${LDAP_BAK_DIR}/rebuild. Exiting." >&2
            exit 1
        fi
        stat --format "Loading config from ldif '%n' modified on %y" ${file_rebuild_config}
        # purge stale data and config
        find ${LDAP_MDB_DIR} -type f -name '*.mdb' -delete
        rm -rf ${LDAP_CONF_DIR}/*
        # load config
        slapadd -v -F ${LDAP_CONF_DIR} -bcn=config -l ${file_rebuild_config}
        # test data load success
        if [ "$?" -ne 0 ]; then
            echo "Config data load did not succeed (return code $?). Exiting." >&2
            exit 1
        fi
        # load data after optionally unzipping
        stat --format "Found data file '%n' modified on %y" ${file_rebuild_data}
        if [[ ${file_rebuild_data} =~ \.gz$ ]]; then
            echo "decompressing..."
            gunzip ${file_rebuild_data}
            data_ldif=${file_rebuild_data/.gz/.ldif}
        else
            data_ldif=${file_rebuild_data}
        fi
        echo "Loading data from '${data_ldif}'"
        start=$SECONDS
        slapadd -F ${LDAP_CONF_DIR} -b ${LDAP_BASE} -l ${data_ldif}
        # test data load success
        if [ "$?" -ne 0 ]; then
            echo "Data load did not succeed (return code $?). Exiting." >&2
            exit 1
        fi
        finish=$SECONDS
        runtime=$(echo "scale=2; $((finish-start)) / 60" | bc)
        echo "Data load completed in approximately $runtime minutes"
        rm -rf ${file_rebuild_force}
    fi
}

function mdb_dump() {

    if [ ! $(which /usr/bin/mdb_copy) ]; then
        echo "mdb_copy not installed. Exiting." >&2
        exit 1
    fi

    if [ ! -d ${LDAP_BAK_DIR}/mdb ]; then
        echo "data dump directory not present (creating): ${LDAP_BAK_DIR}/mdb" >&2
        mkdir -p ${LDAP_BAK_DIR}/mdb
    fi

    if [ ! -d ${LDAP_BAK_DIR}/mdb/accesslog ]; then
        echo "accesslog dump directory not present (creating): ${LDAP_BAK_DIR}/mdb/accesslog" >&2
        mkdir -p ${LDAP_BAK_DIR}/mdb/accesslog
    fi

    if [ ! -d ${LDAP_BAK_DIR}/config ]; then
        echo "config dump directory not present (creating): ${LDAP_BAK_DIR}/config" >&2
        mkdir -p ${LDAP_BAK_DIR}/config
    fi

    # exit w/code 0 unless no snapshot has been taken within the MDB_DUMP_FREQ
    bak=$(ls -t ${LDAP_BAK_DIR}/mdb/* | head -n 1)
    bak_from_epoch=$(stat -c %Y $bak)
    now_from_epoch=$(date +%s)
    bak_from_now=$(expr $now_from_epoch - $bak_from_epoch)

    if [ $MDB_DUMP_FREQ -gt $bak_from_now ]; then
        echo "Recent snapshot not older than $MDB_DUMP_FREQ seconds. Skipping this run."
        return 0
    else
        echo "Recent snapshot is older than $MDB_DUMP_FREQ seconds."
    fi

    timestamp=$(date "+%Y%m%d%H%M%S")
    tmpdir=/tmp/$timestamp
    mkdir -p $tmpdir
    # data mdb
    echo "Performing snapshot using mdbcopy"
    /usr/bin/mdb_copy ${LDAP_MDB_DIR} ${LDAP_BAK_DIR}/mdb
    mv ${LDAP_BAK_DIR}/mdb/data.mdb{,.$timestamp}
    ls -lt ${LDAP_BAK_DIR}/mdb/data.mdb.${timestamp}
    # accesslog mdb
    if [[ ${LDAP_ROLE} == "master" ]]; then
        echo "Performing accesslog snapshot using mdbcopy"
        /usr/bin/mdb_copy ${LDAP_MDB_DIR}/accesslog ${LDAP_BAK_DIR}/mdb/accesslog
        mv ${LDAP_BAK_DIR}/mdb/accesslog/{data.mdb,accesslog.mdb.${timestamp}}
        ls -lt ${LDAP_BAK_DIR}/mdb/accesslog/accesslog.mdb.${timestamp}
    fi
    # config
    echo "Performing config dump using slapcat"
    slapcat -F /etc/ldap/slapd.d -b cn=config > ${tmpdir}/config-${LDAP_ROLE}.ldif.$timestamp
    # ensure slapcat issued no errors before copying config to backups
    if [ "$?" -ne 0 ]; then
        echo "Config dump did not succeed. Exiting." >&2
        exit 1
    fi
    cp -v ${tmpdir}/config-${LDAP_ROLE}.ldif.$timestamp ${LDAP_BAK_DIR}/config
    # clean up
    rm -Rf ${tmpdir}

}

function ldif_dump() {

    echo -e "Performing ldif dump for ${LDAP_ROLE} database"

    # exit if backup directories not present
    if [ ! -d ${LDAP_BAK_DIR}/ldif ]; then
        echo "ldif dump directory not present: ${LDAP_BAK_DIR}/ldif" >&2
        exit 1
    fi
    # wait for slapd process to exit before backing up
    j=0
    while pgrep -x slapd; do
        sleep 3
        j=$(($j+1))
        if [ $j -gt 40 ]; then
            echo "slapd shutdown is hung so backup cannot continue. Exiting." >&2
            exit 1
        fi
    done
    # dump and zip
    timestamp=$(date "+%Y%m%d%H%M%S")
    slapcat -v -b ${LDAP_BASE} > ${LDAP_BAK_DIR}/ldif/${LDAP_ROLE}-${timestamp}.db-ldif
    # ensure slapcat issued no errors
    if [ "$?" -ne 0 ]; then
        echo "Database dump did not succeed. Exiting." >&2
        exit 1
    fi
    gzip -v ${LDAP_BAK_DIR}/ldif/${LDAP_ROLE}-${timestamp}.db-ldif
}

function prune_dumpdir() {

    # data dump dir
    if [ ! -d ${LDAP_BAK_DIR}/mdb ]; then
        echo "data dump directory not present (creating): ${LDAP_BAK_DIR}/mdb" >&2
        mkdir -p ${LDAP_BAK_DIR}/mdb
    fi

    files=($(ls -t ${LDAP_BAK_DIR}/mdb/data.mdb*))
    file_count=${#files[@]}
    trim_num=$(expr ${file_count} - ${MDB_BAK_HISTORY})

    if [ $trim_num -gt 0 ]; then
        for (( i=$MDB_BAK_HISTORY; i<=$(( $file_count -1 )); i++ ))
        do
            #echo -e "deleting file: ${files[$i]}\n"
            rm -v ${files[$i]}
        done
    else
        echo "No files to remove in ${LDAP_BAK_DIR}/mdb"
    fi

    # accesslog dump dir
    if [ ! -d ${LDAP_BAK_DIR}/mdb/accesslog ]; then
        echo "accesslog dump directory not present (creating): ${LDAP_BAK_DIR}/mdb/accesslog" >&2
        mkdir -p ${LDAP_BAK_DIR}/mdb/accesslog
    fi

    files=($(ls -t ${LDAP_BAK_DIR}/mdb/accesslog/accesslog.mdb*))
    file_count=${#files[@]}
    trim_num=$(expr ${file_count} - ${MDB_BAK_HISTORY})

    if [ $trim_num -gt 0 ]; then
        for (( i=$MDB_BAK_HISTORY; i<=$(( $file_count -1 )); i++ ))
        do
            #echo -e "deleting file: ${files[$i]}\n"
            rm -v ${files[$i]}
        done
    else
        echo "No files to remove in ${LDAP_BAK_DIR}/mdb/accesslog"
    fi

    # config dir 
    if [ ! -d ${LDAP_BAK_DIR}/config ]; then
        echo "config dump directory not present (creating): ${LDAP_BAK_DIR}/config" >&2
        mkdir -p ${LDAP_BAK_DIR}/config
    fi

    files=($(ls -t ${LDAP_BAK_DIR}/config/*.ldif*))
    file_count=${#files[@]}
    trim_num=$(expr ${file_count} - ${CONF_BAK_HISTORY})

    if [ $trim_num -gt 0 ]; then
        for (( i=$CONF_BAK_HISTORY; i<=$(( $file_count -1 )); i++ ))
        do
            #echo -e "deleting file: ${files[$i]}\n"
            rm -v ${files[$i]}
        done
    else
        echo "No files to remove in ${LDAP_BAK_DIR}/config"
    fi

}

function kinit_svc() {

    ccfile=${KRB5CCNAME#*:}

    if [[ ${HOSTNAME} =~ 'cluster.local' ]]; then
         principal=${HOSTNAME%%.*}.${REALM}
    else
        principal=${HOSTNAME%%.*}1.${REALM}
    fi
    kinit -k -t $KRB5_KTNAME -c ${ccfile} ldap/${principal}
    klist

}

case "${ACTION}" in
  preflight)
    mdb_load && kinit_svc
    ;;

  dump)
    mdb_dump && prune_dumpdir
    ;;

  ldifdump)
    ldif_dump
    ;;

  syncfg)
    ingest_config
    ;;

  krenew)
    kinit_svc
    ;;

  datapop)
    mdb_load
    ;;

  ldap)
    /usr/sbin/slapd -h ldap:/// -F /etc/ldap/slapd.d -d ${LDAP_LOG_LEVEL}
    ;;

  stop)
    pid=$(cat /var/run/slapd.pid 2>/dev/null || pidof slapd)
    retry=TERM/20/KILL/forever
    if [ -n $pid ]; then
      start-stop-daemon --stop --retry ${retry} --pid ${pid} 2>&1
    else
      start-stop-daemon --stop --retry ${retry} --exec /usr/sbin/slapd 2>&1
    fi
    ;;

  *)
    echo "Usage: $0 (ldap|preflight|dump|ldifdump|ingestconfig|krenew)" >&2
    exit 1
    ;;
esac
