#!/bin/bash -e
#
# Register with AWS route53 service.
# Multivalued A-records
#


if [ $# -ne 7 ];
then
 echo "$0 -d|-u -z <zone> -n <node_name> -a <address_array>"
 exit 1
fi

# . /etc/aws/account.envvars

while getopts ":duz:n:a:" OPTION
do
    case $OPTION in
        d)
          action="DELETE"
          ;;
        u)
          action="UPSERT"
          ;;
        n)
          node_name=$OPTARG
          ;;
        z)
          zone_name=$OPTARG
          ;;
        a)
          ip_array=$OPTARG
          ip_a=( $ip_array )
          ;;
        ?)
          echo "$0 -d|-u -z <zone> -n <node_name> -a <address_array>"
          exit
          ;;
    esac
done

node_name=${node_name/.*/}.${zone_name}
timestamp=$(date +%Y%m%d%H%M%s)

# Deployment key file if not using role-based policy
# AWS_CONFIG_ENV=/root/.aws/envvars

if [ "$action" = "DELETE" ];
then
  # Find the current registered IP and deregister it
  address=$(host $node_name | grep -Eo "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
fi


# Zone to register

hosted_zone_id=$(aws route53 list-hosted-zones | jq --arg name "$zone_name." -r '.HostedZones | .[] | select(.Name=="\($name)") | .Id' | sed 's#/hostedzone/##')

for ((i=0; i<${#ip_a[@]}-1; i++)); do
cat << EOF | tee -a /tmp/$timestamp
                  {
                      "Value": "${ip_a[$i]}"
                  },
EOF
done
cat << EOF | tee -a /tmp/$timestamp
                  {
                    "Value": "${ip_a[-1]}"
                  }
EOF

resource_recs=$(cat /tmp/$timestamp)
rm -f /tmp/$timestamp

cat > /tmp/$node_name.json <<CHANGESET
{
    "Comment": "Updated by $0",
    "Changes": [
        {
            "Action": "${action}",
            "ResourceRecordSet": {
                "Name": "${node_name}",
                "Type": "A",
                "TTL": 60,
                "ResourceRecords": [
                    $resource_recs
                ]
            }
        }
    ]
}
CHANGESET

aws route53 change-resource-record-sets --hosted-zone-id $hosted_zone_id --change-batch file:///tmp/$node_name.json