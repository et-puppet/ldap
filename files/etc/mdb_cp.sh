#!/bin/bash

LDAP_MDB_DIR=${1:-LDAP_MDB_DIR}
LDAP_BAK_DIR=${2:-LDAP_BAK_DIR}
MDB_BAK_HISTORY=${3:-MDB_BAK_HISTORY}
LDAP_ROLE=${4:-LDAP_ROLE}
dump_secs=${5:-14400}

start_offset=$(seq 1200 1500 | sort -R | head -n 1)  # random offset for initial snapshot so that co-deployed containers aren't dumping ~simulataneously
init_dump_secs=$(( $dump_secs - $start_offset ))

echo "performing database snapshot for $instance every $dump_secs seconds, with initial snapshot occuring in $init_dump_secs seconds"

function mdb_cp() {

    if [ ! $(which /usr/bin/mdb_copy) ]; then
        echo "mdb_copy not installed. Exiting." >&2
        exit 1
    fi

    if [ ! -d ${LDAP_BAK_DIR} ]; then
        echo "data dump directory not present (creating): ${LDAP_BAK_DIR}" >&2
        mkdir -p ${LDAP_BAK_DIR}
    fi

    if [[ ${LDAP_ROLE} == "master" && ! -d ${LDAP_BAK_DIR}/accesslog ]]; then
        echo "accesslog dump directory not present (creating): ${LDAP_BAK_DIR}/accesslog" >&2
        mkdir -p ${LDAP_BAK_DIR}/accesslog
    fi

    timestamp=$(date "+%Y%m%d%H%M%S")
    # data mdb
    echo "Performing snapshot using mdbcopy"
    echo "/usr/bin/mdb_copy ${LDAP_MDB_DIR} ${LDAP_BAK_DIR}"
    /usr/bin/mdb_copy ${LDAP_MDB_DIR} ${LDAP_BAK_DIR}
    mv ${LDAP_BAK_DIR}/data.mdb{,.$timestamp}
    ls -lt ${LDAP_BAK_DIR}/data.mdb.${timestamp}
    # accesslog mdb
    if [[ ${LDAP_ROLE} == "master" ]]; then
        echo "Performing accesslog snapshot using mdbcopy"
        /usr/bin/mdb_copy ${LDAP_MDB_DIR}/accesslog ${LDAP_BAK_DIR}/accesslog
        mv ${LDAP_BAK_DIR}/accesslog/{data.mdb,accesslog.mdb.${timestamp}}
        ls -lt ${LDAP_BAK_DIR}/accesslog/accesslog.mdb.${timestamp}
    fi
}

function prune_dumps() {

    # data dump dir
    if [ ! -d ${LDAP_BAK_DIR} ]; then
        echo "data dump directory not present (creating): ${LDAP_BAK_DIR}" >&2
        mkdir -p ${LDAP_BAK_DIR}
    fi

    files=($(ls -t ${LDAP_BAK_DIR}/data.mdb*))
    file_count=${#files[@]}
    trim_num=$(expr ${file_count} - ${MDB_BAK_HISTORY})

    if [ $trim_num -gt 0 ]; then
        for (( i=$MDB_BAK_HISTORY; i<=$(( $file_count -1 )); i++ ))
        do
            #echo -e "deleting file: ${files[$i]}\n"
            rm -v ${files[$i]}
        done
    else
        echo "No files to remove in ${LDAP_BAK_DIR}"
        echo "file count: $file_count"
        echo "mdb history: $MDB_BAK_HISTORY"
    fi

    # accesslog dump dir
    if [[ ${LDAP_ROLE} == "master" ]]; then
        if [ ! -d ${LDAP_BAK_DIR}/accesslog ]; then
            echo "accesslog dump directory not present (creating): ${LDAP_BAK_DIR}/accesslog" >&2
            mkdir -p ${LDAP_BAK_DIR}/accesslog
        fi

        files=($(ls -t ${LDAP_BAK_DIR}/accesslog/accesslog.mdb*))
        file_count=${#files[@]}
        trim_num=$(expr ${file_count} - ${MDB_BAK_HISTORY})

        if [ $trim_num -gt 0 ]; then
            for (( i=$MDB_BAK_HISTORY; i<=$(( $file_count -1 )); i++ ))
            do
                #echo -e "deleting file: ${files[$i]}\n"
                rm -v ${files[$i]}
            done
        else
            echo "No files to remove in ${LDAP_BAK_DIR}/accesslog"
            echo "file count: $file_count"
            echo "mdb history: $MDB_BAK_HISTORY"
        fi
    fi
}

sleep $init_dump_secs
while true
do
    mdb_cp
    prune_dumps
    sleep $dump_secs
done

