#!/bin/bash -e
#
# Register with AWS route53 service.
# Suport for A (ipv4) record only. Can be easily changed to support other types.
#
# Forked from Original by Xueshan Feng <sfeng@stanford.edu> 
# to remove container exec and use JQ instead of grep, and use updated 'list-hosted-zones' command
# 

if [ $# -ne 5 ];
then
 echo "$0 -d|-u -z <zone> -n <node_name>"
 exit 1
fi

# . /etc/aws/account.envvars

while getopts ":duz:n:" OPTION
do
    case $OPTION in
        d)
          action="DELETE"
          ;;
        u)
          action="UPSERT"
          ;;
        n)
          node_name=$OPTARG
          ;;
        z)
          zone_name=$OPTARG
          ;;
        ?)
          echo "$0 -d|-u -z <zone> -n <node_name>"
          exit
          ;;
    esac
done

node_name=${node_name/.*/}.${zone_name}

# Deployment key file if not using role-based policy
# AWS_CONFIG_ENV=/root/.aws/envvars

if [ "$action" = "DELETE" ];
then
  # Find the current registered IP and deregister it
  address=$(host $node_name | grep -Eo "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
else
  # Address to register
  if [ "X$zone_name" = "Xcluster.local" ];
  then
    address=$(/usr/bin/wget -q -O - http://169.254.169.254/latest/meta-data/local-ipv4)
  else
    address=$(/usr/bin/wget -q -O - http://169.254.169.254/latest/meta-data/public-ipv4)
  fi
fi

# Zone to register

hosted_zone_id=$(aws route53 list-hosted-zones | jq --arg name "$zone_name." -r '.HostedZones | .[] | select(.Name=="\($name)") | .Id' | sed 's#/hostedzone/##')

cat > /tmp/$node_name.json <<CHANGESET
{
    "Comment": "Updated by $0",
    "Changes": [
        {
            "Action": "${action}",
            "ResourceRecordSet": {
                "Name": "${node_name}",
                "Type": "A",
                "TTL": 60,
                "ResourceRecords": [
                    {
                        "Value": "${address}"
                    }
                ]
            }
        }
    ]
}
CHANGESET

aws route53 change-resource-record-sets --hosted-zone-id $hosted_zone_id --change-batch file:///tmp/$node_name.json
