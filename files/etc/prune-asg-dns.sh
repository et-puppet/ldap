#!/bin/bash

#asg_name=ldap-${LDAP_ENV}-${LDAP_POOL}
asg_name=ldap-${LDAP_ENV}
upsert_freq=300

init_wait_secs=$(seq 30 120 | sort -R | head -n 1)  # random offset for initial upsert so that co-deployed instances aren't checking ~simulataneously

sleep $init_wait_secs

while true
    do
    # all the in-service instances in the ASG 
    ids=$( aws autoscaling describe-auto-scaling-instances --region $AWS_DEFAULT_REGION --output text --query \
        "AutoScalingInstances[?contains(AutoScalingGroupName,\`$asg_name\`)==\`true\`] | \
        [?LifecycleState == \`InService\`].InstanceId" )

    # these instances' PublicIpAddress
    ips=$( aws ec2 describe-instances --region $AWS_DEFAULT_REGION --instance-ids $ids --query \
        'Reservations[].Instances[].PublicIpAddress' --output text )

    # upsert multivalued A-record
    /usr/bin/r53-register-multi.sh -u -z ${LDAP_ENV}.${ZONE_PARENT} -n ldap-pool-${LDAP_POOL} -a "$ips"

    # sleep
    sleep $upsert_freq
done

