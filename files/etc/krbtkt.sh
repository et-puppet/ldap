#!/bin/bash

principal=$K5_PRINCIPAL

spn=ldap/$principal
renew_secs=7200
K5_KEYPATH=/etc/ldap.keytab
ccfile=FILE:/tmp/krb5cc_0


echo "renewing kerberos tgt for $spn every $renew_secs seconds"
while true
do
    kinit -k -t $K5_KEYPATH -c $ccfile $spn
    klist
    sleep $renew_secs
done

